const initialState = ''

const title = (state = initialState, action) => {
    switch (action.type) {
        case 'UPDATE_TITLE':
            return action.payload
        case 'ADD_TODO':
            return initialState
        default:
            return state
    }
}

export default title